#ifndef VIZINHOS_H
#define VIZINHOS_H

#include "../libs/matriz.h" //struct Pos
#include <vector>
using namespace std;

//left, up, right, down
vector<Par> DESL_LATERAL =   {Par(0, -1), Par(-1, 0), Par(0, 1), Par(1, 0)};
//left-up, right-up, right-down, left-down
vector<Par> DESL_QUINAS = {Par(-1, -1), Par(-1, 1), Par(1,1), Par(1, -1)};

//left, left-up, up, up-right ...
vector<Par> DESL_TODOS = {Par( 0,-1), Par(-1,-1),
                          Par(-1, 0), Par(-1, 1),
                          Par( 0, 1), Par( 1, 1),
                          Par( 1, 0), Par( 1,-1)};

//embaralha um vetor de inteiros
template <class T>
void shuffle(vector<T>& vi){
    for(int i = 0; i < (int)vi.size(); i++)
        std::swap(vi[i], vi[rand()%vi.size()]);
}

vector<Par> get_vizinhos(Par pos, vector<Par> desloc = DESL_LATERAL){
    vector<Par> viz;
    for(size_t i = 0; i < desloc.size(); i++)
        viz.push_back(Par(pos.l + desloc[i].l, pos.c + desloc[i].c));
    return viz;
}

#endif
