#ifndef CONST_LAB_REC_H
#define CONST_LAB_REC_H

#include "../libs/player.h"
#include "../libs/matriz.h"
#include "../libs/view.h"

#include "vizinhos.h"

#include <fstream>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <stack>
#include "file_mat.h"

using namespace std;

namespace lab_create{

    MatrizView mview;

    void pintar_matriz(matchar mat, stack<Par> pilha, vector<Par> eleg = vector<Par>()){
        while(!pilha.empty()){
            mat.get(pilha.top()) = 'b';
            pilha.pop();
        }

        for(auto pos : eleg)
            mat.get(pos) = 'r';
        my_view->paint(mat);
    }

    bool eh_elegivel(const matchar & mat, Par candidato){
        if(mat.equals(candidato, '#')){
            int qtd_side = 0;
            //contando quantos vizinhos furados o candidato possui
            for(Par viz : get_vizinhos(candidato)){
                if(!mat.is_inside(viz) || mat.equals(viz, ' '))
                    qtd_side++;
            }

            //se ele tiver 1 ou menos furados ele eh elegivel
            if(qtd_side <= 1){

                //comente o bloco abaixo para ter quinas
                auto viz_all = get_vizinhos(candidato, DESL_TODOS);
                for(int i = 1; i < 8; i+=2){
                    if(mat.equals(viz_all[i], ' ') &&
                       mat.equals(viz_all[i - 1], '#') &&
                       mat.equals(viz_all[(i + 1) % 8], '#'))
                        return false;
                }

                return true;
            }
        }
        return false;
    }


    //########################## RECURSIVE CREATION

    void _create_rec(matchar & mat, Par actual, bool view){
        if(!mat.is_inside(actual))
            return;

        if(eh_elegivel(mat, actual)){
            mat.get(actual) = ' ';
            if(view){
                my_view->paint(mat);
                my_view->paint(mat, actual, 'g');
                my_view->show();
            }

            auto vizs = get_vizinhos(actual);
            shuffle<Par>(vizs);
            for(auto v : vizs)
                _create_rec(mat, v, view);

            if(view){
                my_view->paint(mat);
                my_view->paint(mat, actual, 'r');
                my_view->show();
            }
        }

    }

    matchar create_rec(int nl = 30, int nc = 45, bool view = true){
        my_view->set_autopush(false);
        srand(time(NULL));
        my_view->set_autoplay(true);
        matchar mat(nl, nc, '#');
        _create_rec(mat, Par(1, 1), view);
        if(view)
            my_view->wait();
        return mat;
    }

    //#############################################################

    matchar create_stack(int nl = 30, int nc = 45, bool view = true){
        srand(time(NULL));
        my_view->set_autoplay(true);
        stack<Par> pilha;
        Par init(1, 1);
        pilha.push(init);
        matchar mat(nl, nc, '#');
        mat.get(init) = ' ';
        if(view)
            pintar_matriz(mat, pilha);

        while(!pilha.empty()){
            vector<Par> elegiveis;

            //ver quem do topo eh elegivel
            for(Par candidato : get_vizinhos(pilha.top())){
                if(eh_elegivel(mat, candidato))
                    elegiveis.push_back(candidato);
            }
            if(view)
                pintar_matriz(mat, pilha, elegiveis);
            if(elegiveis.size() == 0){
                pilha.pop();
            }else{
                Par eleito = elegiveis[rand() % elegiveis.size()];
                pilha.push(eleito);
                mat.get(eleito) = ' ';
                if(view)
                    pintar_matriz(mat, pilha);
            }
        }
        if(view){
            my_view->paint(mat);
            my_view->wait();
        }
        //file_save_mat("labP.txt", mat);
        return mat;
    }

}

#endif // CONST_LAB_REC_H
