#ifndef FLOODFILL_H
#define FLOODFILL_H

#include "vizinhos.h"
#include "../libs/player.h"
#include "../libs/matriz.h"
#include "../libs/view.h"

namespace floodfill{
const char MARCADOR = '.';
const int linhas = 30;
const int colunas = 60;
matchar mat(linhas, colunas, 'w');//inicia matriz toda em preto
matriz<int> matnum(linhas,colunas, -1);

void flood(Par init){
    char cor = mat.get(init.l, init.c);
    list<Par> fila;
    mat.get(init.l, init.c) = MARCADOR;
    fila.push_back(init);

    while(!fila.empty()){
        //retira o elemento marcando todos que tiverem ao lado dele
        //da mesma cor e colocando-os na fila
        Par ref = fila.front();//o elemento em referencia
        fila.pop_front();
        //get_vizinhos está em comum.h
        for(Par viz : get_vizinhos(ref, DESL_TODOS)){//os vizinhos em todas dir
            if(mat.is_inside(viz)){//se dentro da matriz
                if(mat.get(viz) == cor){//se da mesma cor
                    mat.get(viz) = MARCADOR;//marca
                    fila.push_back(viz);//empilha

                    //mudando valores para vizualizar melhor
                    mat.get(ref) = 'y'; //referencia de vermelho
                    for(auto p : fila)
                        mat.get(p) = 'g';//pilha de azul

                    my_view->paint(mat);

                    //retornando os valores iniciais
                    mat.get(ref) = MARCADOR;
                    for(auto p : fila)
                        mat.get(p) = MARCADOR;

                }
            }
        }
    }
}

#include "file_mat.h"
void pintar_e_preencher(){
    //cor que vamos marcar os elementos
    //variavel definida em player.h
    my_view->paint_brush(mat, "kwrb");//permite pintar de preto e branco

    //mat = file_load_mat("labG.txt");
    Par init = my_view->select_point(mat, "Escolha um ponto inicial para pintar");

    //my_player controla o visualizador
    my_view->set_autoplay(true);
    flood(init);
    my_view->paint(mat);
    //avisa que a simulacao acabou
    my_view->wait();
}

}

#endif // FLOODFILL_H
