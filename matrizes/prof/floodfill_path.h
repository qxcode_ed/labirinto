#ifndef FLOODFILLPATH_H
#define FLOODFILLPATH_H

#include "vizinhos.h"
#include "../libs/player.h"
#include "../libs/matriz.h"
#include "../libs/view.h"


namespace floodfill_path{

const char MARCADOR = '.';

void encontrar_caminho(matchar &mat, matriz<int> &nums, Par init, Par end){
    mat.get(end) = 'y';
    my_view->paint(mat, nums);

    if(end == init)
        return;

    for(auto viz : get_vizinhos(end)){
        if(nums.is_inside(viz))
            if(nums.get(viz) == (nums.get(end) - 1)){
                encontrar_caminho(mat, nums, init, viz);
                return;
            }
    }
}

bool flood(matchar &mat, matriz<int> &nums, Par init, Par end){
    char cor = mat.get(init.l, init.c);
    list<Par> fila;
    mat.get(init.l, init.c) = MARCADOR;
    nums.get(init) = 0;

    fila.push_back(init);

    while(!fila.empty()){
        //retira o elemento marcando todos que tiverem ao lado dele
        //da mesma cor e colocando-os na fila
        Par ref = fila.front();//o elemento em referencia
        fila.pop_front();
        //get_vizinhos está em comum.h
        for(Par viz : get_vizinhos(ref)){//os vizinhos em todas dir
            if(mat.is_inside(viz)){//se dentro da matriz
                if(mat.get(viz) == cor){//se da mesma cor
                    mat.get(viz) = MARCADOR;//marca


                    nums.get(viz) = nums.get(ref) + 1;        //ADD
                    fila.push_back(viz);//empilha

                    //mudando valores para vizualizar melhor
                    mat.get(ref) = 'r'; //referencia de vermelho
                    for(auto p : fila)
                        mat.get(p) = 'y';//pilha de azul

                    my_view->paint(mat, nums);

                    //retornando os valores iniciais
                    mat.get(ref) = MARCADOR;
                    for(auto p : fila)
                        mat.get(p) = MARCADOR;


                    if(viz.l == end.l && viz.c == end.c)
                       encontrar_caminho(mat, nums, init, end);
                }
            }
        }
    }
    return false;
}




void pintar_e_preencher(int linhas = 30, int colunas = 45){
    matchar mat(linhas, colunas, 'w');//inicia matriz toda em preto
    matriz<int> nums(linhas, colunas, -1);

    my_view->paint_brush(mat, "kwrb");//permite pintar de preto e branco
    Par init = my_view->select_point(mat, "Escolha um ponto para iniciar");
    Par end = my_view->select_point(mat, "Escolha um ponto para terminar");
    //my_player controla o visualizador
    my_view->set_autoplay(true);

    flood(mat, nums, init, end);

    //avisa que a simulacao acabou
    my_view->wait();
}

#include "lab_create.h"
void flood_on_lab(int linhas = 30, int colunas = 45){

    matchar mat = lab_create::create_stack(linhas, colunas, false);
    matriz<int> nums(linhas, colunas, -1);

    Par init = my_view->select_point(mat, "Escolha um ponto para iniciar");
    Par end = my_view->select_point(mat, "Escolha um ponto para terminar");
    flood(mat, nums, init, end);
    my_view->wait();
}

}//namespace

#endif // FLOODFILL_H
