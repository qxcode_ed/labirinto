#ifndef QUEIMADA_SIMPLES_H
#define QUEIMADA_SIMPLES_H

#include "../libs/player.h"
#include "../libs/matriz.h"
#include "../libs/view.h"
#include "vizinhos.h"

#include <fstream>
#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

namespace queimada_simples{
    int queimar(matchar &mat, matriz<int> &nums, Par pos, int nivel, bool aleatorio, bool numerica){
        if(!mat.is_inside(pos))
            return 0;

        if(mat.get(pos) == 'g'){
            mat.get(pos) = 'r';
            nums.get(pos) = nivel;
            if(numerica)
                my_view->paint(mat, nums);
            else
                my_view->paint(mat);
            int qtd = 1;
            auto vizinhos = get_vizinhos(pos);
            if(aleatorio)
                shuffle<Par>(vizinhos);
            for(auto viz : vizinhos)
                qtd += queimar(mat, nums, viz, nivel + 1, aleatorio, numerica);

            mat.get(pos) = 'y';

            if(numerica)
                my_view->paint(mat, nums);
            else
                my_view->paint(mat);

            return qtd;
        }

        return 0;
    }

    void queimada_main(bool aleatorio, bool numerica){
        int nl = 20;
        int nc = 35;
        matchar mat(nl, nc, 'g');
        matriz<int> nums(nl, nc, 0);
        my_view->paint_brush(mat, "wg");
        Par pos = my_view->select_point(mat, "Escolha um ponto verde da matriz para iniciar o fogo.");
        my_view->set_autoplay(true);
        auto cont = queimar(mat, nums, pos, 0, aleatorio, numerica);
        cout << "Queimaram " << cont << " arvores \n";

        my_view->wait();
    }
}


#endif
