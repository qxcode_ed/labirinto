#ifndef LAB_SOLVE_STACK_H
#define LAB_SOLVE_STACK_H

#include <cstdlib>
#include <time.h>

#include "lab_create.h"
#include "../libs/view.h"
#include "../libs/matriz.h"
#include "vizinhos.h"

#include <stack>

namespace lab_solve {

    const char VISITADO = '.';
    const char PILHA = 'c';

    void print_mat(matchar mat, Par init, Par fim, stack<Par> pilha = stack<Par>()){
        if(!pilha.empty()){
            mat.get(pilha.top()) = 'g';
            pilha.pop();
        }

        while(!pilha.empty()){
            mat.get(pilha.top()) = PILHA;
            pilha.pop();
        }

        mat.get(init) = 'g';
        mat.get(fim) = 'r';
        my_view->paint(mat);
    }


    //##################  STACK SOLUTION ##########################

    void solve_stack(matchar& mat, Par init, Par fim){
        stack<Par> pilha;
        pilha.push(init);
        mat.get(init) = VISITADO;//ja foi visitado
        while(!pilha.empty()){
            Par top = pilha.top();
            if(top == fim)
                break;
            vector<Par> candidatos;
            for(Par viz : get_vizinhos(top))
                if(mat.equals(viz, ' '))
                    candidatos.push_back(viz);
            if(candidatos.size() != 0){
                Par new_top = candidatos[rand() % candidatos.size()];
                mat.get(new_top) = VISITADO;
                pilha.push(new_top);
            }else{
                pilha.pop();
            }
            print_mat(mat, init, fim, pilha);
        }
    }

    void solve_stack_main(){
        srand(time(NULL));
        matchar mat = lab_create::create_stack(20, 30, false);
        Par init = my_view->select_point(mat, "Escolha o inicio");
        mat.get(init) = 'g';
        Par fim = my_view->select_point(mat, "Escolha o fim");
        mat.get(fim) = 'r';

        my_view->paint(mat);

        mat.get(init) = ' ';//zerando as marcacoes visuais
        mat.get(fim) = ' ';
        solve_stack(mat, init, fim);
        my_view->wait();

    }

    //##################  RECURSIVE SOLUTION ##########################

    const char PATH = 'y';

    bool solve_rec(matchar& mat, Par actual, Par fim){
        if(!mat.is_inside(actual))
            return false;

        if(actual == fim){
            mat.get(actual) = PATH;
            print_mat(mat, actual, fim);
            return true;
        }

        if(mat.equals(actual, ' ')){
            mat.get(actual) = VISITADO;
            print_mat(mat, actual, fim);
            for(Par viz : get_vizinhos(actual)){
                if(solve_rec(mat, viz, fim)){//se o vizinho for path, ele tambem eh path
                    mat.get(actual) = PATH;
                    print_mat(mat, actual, fim);
                    return true;
                }
            }
        }
        return false;
    }

    void solve_rec_main(){
        srand(time(NULL));
        matchar mat = lab_create::create_stack(20, 30, false);
        Par init = my_view->select_point(mat, "Escolha o inicio");
        mat.get(init) = 'g';
        Par fim = my_view->select_point(mat, "Escolha o fim");
        mat.get(fim) = 'r';

        my_view->paint(mat);

        mat.get(init) = ' ';//zerando as marcacoes visuais
        mat.get(fim) = ' ';
        solve_rec(mat, init, fim);
        my_view->wait();
    }



}


#endif // LAB_SOLVE_STACK_H
