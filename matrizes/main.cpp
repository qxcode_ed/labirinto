#include "prof/lab_create.h"
#include "prof/queimada_simples.h"
#include "prof/floodfill.h"
#include "prof/floodfill_path.h"
#include "prof/file_mat.h"
#include "prof/lab_solve.h"

#include "libs/view.h"
#include "aluno/jogo_velha.h"
#include "aluno/exemplo_lab.h"
#include "aluno/teste_matrizes.h"

int main(){
    //ALUNO
    //aluno::queimada_aluno();
    //jogo da velha
    //velha_main();
    //teste_matrizes();

    //PROF
    int num = 0;
    while(num != 10 and MyWindow::instance()->isOpen()){
        cout << "Digite uma opcao ou 10 para sair:" << endl <<
                "0 - preenchimento recursivo" << endl <<
                "1 - preenchimento recursivo aleatorio" << endl <<
                "2 - preenchimento recursivo com profundidade" << endl <<
                "3 - criacao de labirinto recursivo" << endl <<
                "4 - criacao de labirinto com pilha" << endl <<
                "5 - solucao labirinto recursivo" << endl <<
                "6 - solucao labirinto com pilha" << endl <<
                "7 - preenchimento floodfill" << endl <<
                "8 - pathfinding floodfill" << endl <<
                "9 - solucao labirinto floodfill" << endl <<
                "10 - sair" << endl;
        cout << ">> ";
        cin >> num;
        if(num == 0)
            queimada_simples::queimada_main(false, false);
        if(num == 1)
            queimada_simples::queimada_main(true, false);
        if(num == 2)
            queimada_simples::queimada_main(true, true);
        if(num == 3)
            lab_create::create_rec(20, 20);
        if(num == 4)
            lab_create::create_stack(20, 30);
        if(num == 5)
            lab_solve::solve_rec_main();
        if(num == 6)
            lab_solve::solve_stack_main();
        if(num == 7)
            floodfill::pintar_e_preencher();
        if(num == 8)
            floodfill_path::pintar_e_preencher();
        if(num == 9)
            floodfill_path::flood_on_lab();
    }
    return 0;
}


